/* Created by HP on 6/16/2018.*/
/* Author: Mainul Hasan.*/



// Select the canvas element in html document
const canvas = document.getElementById("myCanvas");

// Set the canvas to screen width & height
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Create a Drawing Object
const c = canvas.getContext('2d');

const mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2
}

const colors = ['#2185C5', '#7ECEFD', '#FFF6E5', '#FF7F66'];

// Event Listeners
addEventListener('mousemove', function (event) {
    mouse.x = event.clientX;
    mouse.y = event.clientY;
})

addEventListener('resize', () => {
    canvas.width = innerWidth;
    canvas.height = innerHeight;

    init();
})

//***************************************//
//Some useful functions
function randomIntFromRange(min, max) {
    /*
    This function will return a random integer between (min,max).
    For example: if min = 1 & max = 5, then we get a random integer between 1 & 5
    */
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomColor(colorArray) {
    /*
    This function gets random color value from a color array.
    */
    return colorArray[Math.floor(Math.random() * colorArray.length)];
}

function distance(x1, y1, x2, y2) {
    /*
    This function computes the distance between two coordinates (x1,y1) & (x2,y2)
    */
    const xDist = x2 - x1;
    const yDist = y2 - y1;
    return Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));
}

//***************************************//

// Objects
function Object(x, y, radius, color) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.color = color;

    this.update = function () {
        this.draw();
    }

    this.draw = function () {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
        c.closePath();
    }
}


// Implementation
let objects;

function init() {
    objects = [];

    for (let i = 0; i < 400; i++) {
        // objects.push();
    }
}

// Animation Loop
function animate() {
    requestAnimationFrame(animate);
    c.clearRect(0, 0, canvas.width, canvas.height);

    c.fillText('HTML CANVAS BOILERPLATE', mouse.x, mouse.y);
    // objects.forEach(object => {
    //  object.update();
    // });
}

init();
animate();
