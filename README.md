Canvas Boilerplate is the go-to solution for quickly creating modern canvas pieces using ES6 and webpack.
Getting Started

    1) Clone the repo:

      git clone https://gitlab.com/Mainul-Hasan/canvas_boilerplate.git
      
     Command line instructions
     
    2) Git global setup
    
        git config --global user.name "Mohammad Mainul Hasan"
        git config --global user.email "mainul.webdesign@gmail.com"
    
    3) Create a new repository
    
        git clone https://gitlab.com/Mainul-Hasan/canvas_boilerplate.git
        cd canvas_boilerplate
        touch README.md
        git add README.md
        git commit -m "add README"
        git push -u origin master
    
    4) Existing folder
    
        cd existing_folder
        git init
        git remote add origin https://gitlab.com/Mainul-Hasan/canvas_boilerplate.git
        git add .
        git commit -m "Initial commit"
        git push -u origin master
    
    5) Existing Git repository
    
        cd existing_repo
        git remote rename origin old-origin
        git remote add origin https://gitlab.com/Mainul-Hasan/canvas_boilerplate.git
        git push -u origin --all
        git push -u origin --tags  

